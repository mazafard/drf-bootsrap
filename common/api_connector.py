import requests


class RestApiConnector(object):
    """
    This is the base rest api connector
    """

    @staticmethod
    def get(**kwargs):
        timeout = kwargs.pop("timeout", 60)
        response = requests.get(timeout=timeout, **kwargs)
        return response

    @staticmethod
    def post(**kwargs):
        timeout = kwargs.pop("timeout", 60)
        response = requests.post(timeout=timeout, **kwargs)
        return response

    @staticmethod
    def put(**kwargs):
        timeout = kwargs.pop("timeout", 60)
        response = requests.put(timeout=timeout, **kwargs)
        return response

    @staticmethod
    def patch(**kwargs):
        timeout = kwargs.pop("timeout", 60)
        response = requests.patch(timeout=timeout, **kwargs)
        return response

    @staticmethod
    def delete(**kwargs):
        timeout = kwargs.pop("timeout", 60)
        response = requests.delete(timeout=timeout, **kwargs)
        return response
