from celery import shared_task

from common.notifications import SmsNotification, EmailNotification

from django.apps import apps
from backend import settings
from datetime import datetime
import datetime as dt


@shared_task
def send_email(to, subject, html_body):
    EmailNotification.send_email(
        subject,
        to,
        html_message=html_body
    )


@shared_task
def send_manual_email(subject, to_email, message, from_name, from_email):
    EmailNotification.send_email(
        subject,
        to_email,
        html_message=message,
        from_name=from_name,
        from_email=from_email,
    )


@shared_task
def send_sms(to, text):
    SmsNotification.send_sms(
        to,
        text
    )


def get_class(module, kls):
    parts = module.split('.')
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return getattr(m, kls)


@shared_task()
def async_runner(module, class_name, func_name, pk, *args, **kwargs):
    klass = get_class(module, class_name)
    obj = klass.objects.filter(pk=pk).first()
    if not obj:
        print(
            "object not found class:{}, function:{}, pk:{}".format(klass, func_name, pk))
        return
    return getattr(obj, func_name)(*args, **kwargs)


def async_function(func=None, countdown=0):
    def wrap(self, *args, **kwargs):
        if not self.pk:
            raise AssertionError('informal usage!')
        sync = kwargs.pop('sync', False)
        if not sync:
            kwargs.update(sync=True)
            async_runner.apply_async(
                args=(
                         self.__module__,
                         self.__class__.__name__,
                         func.__name__,
                         self.pk
                     ) + args,
                kwargs=kwargs,
                countdown=countdown
            )
        else:
            return func(self, *args, **kwargs)

    if callable(func):
        return wrap
    return lambda function: async_function(function, countdown)



@shared_task
def test():
    print("Celery-beat is working!")
