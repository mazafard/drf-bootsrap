from functools import update_wrapper

from django.urls import path


def build_admin_url(self, url_name, reverse=False):
    prefix = ""
    if reverse:
        prefix = "admin:"
    return '%s%s_%s_%s' % (
        prefix,
        self.model._meta.app_label,
        self.model._meta.model_name,
        url_name
    )


def wrap(self, view):
    def wrapper(*args, **kwargs):
        return self.admin_site.admin_view(view)(*args, **kwargs)

    return update_wrapper(wrapper, view)


def append_routes(*items):
    def decorate_get_urls(cls):
        get_urls = getattr(cls, 'get_urls')

        def new_get_urls(self):
            appended_routes = []
            for item in items:
                func = getattr(self, item)
                assert hasattr(self, item) and callable(func), \
                    "the appended url ({}) is not valid".format(item)

                pattern = item
                if hasattr(func, 'pattern'):
                    pattern = func.pattern

                name = item
                if hasattr(func, 'name'):
                    name = func.name
                appended_routes.append(
                    path(
                        pattern,
                        wrap(self, func),
                        name=build_admin_url(self, name),
                    )
                )
            return get_urls(self) + appended_routes

        setattr(cls, 'get_urls', new_get_urls)
        return cls

    return decorate_get_urls
