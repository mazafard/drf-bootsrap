import phonenumbers as phonenumbers
from rest_framework import serializers
from django.utils.translation import gettext as _


class CheckExistValidator(object):
    def __init__(self, query_set, field='id', message=None):
        self.query_set = query_set
        self.field = field
        self.message = message if message else _(
            'The requested {field_name}({value}) is not exists.')

    def __call__(self, value):
        if not self.query_set.filter(**{self.field: value}).exists():
            message = self.message.format(field_name=self.field, value=value)
            raise serializers.ValidationError(message)


class CheckNotExistValidator(object):
    def __init__(self, query_set, field='id', message=None):
        self.query_set = query_set
        self.field = field
        self.message = message if message else _(
            'The requested {field_name}({value}) is exists.')

    def __call__(self, value):
        if self.query_set.filter(**{self.field: value}).exists():
            message = self.message.format(field_name=self.field, value=value)
            raise serializers.ValidationError(message)


class PhoneNumberValidator(object):
    def __init__(self, message=None):
        self.message = message if message else _(
            'please enter a valid phone number.'
        )

    def __call__(self, value):
        try:
            x = phonenumbers.parse(value, 'IR')
            if not phonenumbers.is_valid_number(x):
                raise serializers.ValidationError(self.message)
        except phonenumbers.NumberParseException as e:
            raise serializers.ValidationError(self.message)
