import random
import string
import uuid
import os
from urllib.parse import urljoin

import shutil
from django.conf import settings
from django.apps import apps
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.files.base import ContentFile
from django.db import models
from django.utils import timezone
from rest_framework.exceptions import NotFound

from common import tasks
from common.notifications import EmailNotification


class BaseModel(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    @classmethod
    def get_by_pk(cls, pk, raise_exception=False) -> 'BaseModel':
        obj = cls.objects.filter(pk=pk).first()
        if raise_exception and not obj:
            raise NotFound('The requested entity is not found')
        return obj

    @classmethod
    def get_all(cls):
        return cls.objects.all()

    @classmethod
    def first_or_create(cls, **kwargs):
        obj = cls.objects.filter(**kwargs).first()
        if not obj:
            obj = cls(**kwargs)
            obj.save()
        return obj

    def load(self, **kwargs):
        for key, value in kwargs.items():
            assert key not in self._meta.fields, \
                """The requested field %s not exists""" % str(key)
            setattr(self, key, value)
        return self


class BaseFileModel(BaseModel):
    key = models.CharField(max_length=127, null=True, blank=True)
    uri = models.CharField(max_length=255, null=True, blank=True)
    extension = models.CharField(max_length=255)
    size = models.PositiveIntegerField(default=0)

    def __init__(self, *args, **kwargs):
        self.file = kwargs.pop('file', None)
        super().__init__(*args, **kwargs)

    class Meta:
        abstract = True

    def get_local_uri(self):
        if self.key is None:
            return ""
        line = str(self.id)
        folders = [line[i:i + 2] for i in range(0, len(line), 2)]

        return os.path.join(
            *folders[:len(folders) - 1] + [
                "{key}.{extension}".format(
                    key=str(self.key),
                    extension=self.extension
                )])

    def get_local_address(self, create_directory=True):
        local_address = os.path.join(settings.UPLOAD_DIRECTORY,
                                     self.get_local_uri())
        if create_directory:
            directory = os.path.dirname(local_address)
            if not os.path.exists(directory):
                os.makedirs(directory)
        return local_address

    def get_url_address(self):
        local_address = self.get_local_uri()
        if local_address != "":
            return urljoin(settings.CDN_DOMAIN, local_address)
        else:
            return ""

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.file:
            self.key = uuid.uuid4() if self.key is None else self.key
            self.extension = self._get_extension()
            self.size = self.file.size
            super().save(force_insert, force_update, using, update_fields)
            self.refresh_from_db()
            self._save_file()
            self.uri = self.get_local_uri()
            force_insert = False
            super().save(force_insert, force_update, using, update_fields)
        else:
            super().save(force_insert, force_update, using, update_fields)

    def _save_file(self):
        with open(self.get_local_address(), str("wb")) as f:
            shutil.copyfileobj(self.file.file, f)

    def delete(self, using=None, keep_parents=False):
        try:
            os.remove(self.get_local_address())
        except OSError:
            pass
        return super().delete(using=using, keep_parents=keep_parents)

    @property
    def url(self):
        return self.get_url_address()

    def __str__(self):
        return self.get_url_address()

    def _get_extension(self):
        if self.extension:
            return self.extension
        return self.file.name.split('.')[-1]

    @property
    def content_file(self):
        file = open(self.get_local_address(False), 'rb')
        content_file = ContentFile(file.read(), name="{key}.{extension}".format(
            key=str(self.key),
            extension=self.extension
        ))
        file.close()
        return content_file


class MinMaxFloat(models.FloatField):
    def __init__(self, min_value=None, max_value=None, *args, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        super(MinMaxFloat, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(MinMaxFloat, self).formfield(**defaults)


class BaseUserModel(AbstractBaseUser):
    user_uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    SYSTEM_TEXT_EMAIL_TYPE = None
    id = models.BigAutoField(
        primary_key=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )
    email = models.EmailField(
        unique=True
    )
    email_verification_code = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    is_email_verified = models.BooleanField(
        default=False
    )
    email_verified_at = models.DateTimeField(blank=True, null=True)
    first_name = models.CharField(
        max_length=255
    )
    last_name = models.CharField(
        max_length=255
    )
    avatar = models.ForeignKey(
        'content.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    USERNAME_FIELD = 'email'

    class Meta:
        abstract = True

    @property
    def username(self):
        return self.email

    @property
    def full_name(self):
        return self.first_name + ' ' + self.last_name

    @property
    def avatar_url(self) -> str or None:
        if self.avatar:
            return self.avatar.url
        return None

    def __str__(self):
        return self.email

    @property
    def is_client_user(self):
        return False

    @property
    def is_freelancer(self):
        return False

    def update_last_login(self):
        self.last_login = timezone.now()
        self.save()

    def generate_email_verification_code(self):
        self.email_verification_code = ''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in
            range(settings.EMAIL_VERIFICATION_CODE_SIZE))
        self.save()

    @tasks.async_function
    def send_verification_email(self, resend=False):
        if not self.email or self.is_email_verified:
            return
        if not resend or not self.email_verification_code:
            self.generate_email_verification_code()

        SystemText = apps.get_model('service.SystemText')
        text = SystemText.get_with_type(
            self.SYSTEM_TEXT_EMAIL_TYPE
        )
        if not text:
            print("Verification email is not set !!")
            return

        text.set_context(user=self)

        EmailNotification.send_email(
            text.title_value,
            self.email,
            html_message=text.text_value,
            from_name=text.subtitle_value
        )

    def get_new_auth_token(self) -> str:
        self.update_last_login()
        AuthToken = apps.get_model('service.AuthToken')
        return AuthToken.get_new_auth_token(self)

    @classmethod
    def get_by_email(cls, email) -> 'BaseUserModel':
        return cls.objects.filter(email=email).first()

    @classmethod
    def get_by_uuid(cls, user_uuid) -> 'BaseUserModel':
        return cls.objects.get(user_uuid=user_uuid)

    @classmethod
    def get_by_email_verification_code(cls, email_verification_code) -> 'BaseUserModel':
        return cls.objects.filter(email_verification_code=email_verification_code).first()
