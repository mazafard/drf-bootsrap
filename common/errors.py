CLIENT_UPDATE_REQUIRED = {
    "status_code": 460,
    "code": 1000,
    "message": "Client update required"
}
INVALID_CHECK_UPDATE_REQUEST = {
    "status_code": 400,
    "code": 1001,
    "message": "You must send X-DEVICE-TYPE and X-APP-VERSION in request headers"
}
METHOD_NOT_ALLOWED = {
    "status_code": 405,
    "code": 405,
    "message": "Request method not allowed"
}

THE_REQUESTED_OBJECT_NOT_FOUND = {
    "status_code": 404,
    "code": 404,
    "message": "The requested object not found"
}

USER_INPUT_IS_NOT_VALID = {
    "status_code": 400,
    "code": 400,
    "message": "The user request is not valid"
}

DUPLICATED_REQUEST = {
    "status_code": 409,
    "code": 409,
    "message": "Duplicated request"
}

YOU_CANNOT_DELETE_THIS_OBJECT_AT_THIS_TIME = {
    "status_code": 403,
    "code": 403,
    "message": "You can't delete this object."
}

YOU_CANNOT_UPDATE_THIS_OBJECT_AT_THIS_TIME = {
    "status_code": 403,
    "code": 403,
    "message": "You can't update this object at this time."
}

YOU_CANNOT_CREATE_OBJECT_AT_THIS_TIME = {
    "status_code": 403,
    "code": 403,
    "message": "You can't create object."
}
