from rest_framework.permissions import BasePermission


class IsAuthenticatedFreelancer(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_freelancer)


class FreelancerCanEditCv(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user.has_pending_cv and (
                request.method.lower() not in ('post', 'put', 'patch', 'delete') or request.user.can_edit_cv
        )


class FreelancerCanEditCvProject(BasePermission):
    def has_object_permission(self, request, view, obj):
        return (request.method.lower() not in ('put', 'patch', 'delete')) or obj.can_edit


class IsAuthenticatedClient(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_client_user)


class FreelancerCanBid(BasePermission):
    def has_permission(self, request, view):
        return request.user.can_bidding


class FreelancerHasActiveCV(BasePermission):
    def has_permission(self, request, view):
        return request.user.has_active_cv
