import base64

from django.core.files.base import ContentFile
from rest_framework import serializers


class BaseSerializer(serializers.Serializer):
    pass


class BaseModelSerializer(serializers.ModelSerializer):
    pass


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str) and data.startswith('data:image'):
            # base64 encoded image - decode
            format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            ext = format.split('/')[-1]  # guess file extension

            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        return super(Base64ImageField, self).to_internal_value(data)


class Base64FileField(serializers.FileField):
    MIME_TYPE_MAP = {
        'application/pdf': 'pdf',
        'image/png': 'png',
        'image/jpeg': 'jpg',
        'image/jpg': 'jpg',
        'application/msword': 'doc',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template': 'docx',
        'application/vnd.ms-word.document.macroEnabled.12,application/vnd.ms-word.template.macroEnabled.12': 'docx',
        'application/vnd.rar': 'rar',
        'application/zip': 'zip',
        'application/x-zip': 'zip',
        'application/octet-stream': 'zip',
        'application/x-zip-compressed': 'zip',
        'application/vnd.ms-powerpoint': 'ppt',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'pptx',
        'application/vnd.openxmlformats-officedocument.presentationml.template': 'potx',
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow': 'ppsx',
        'application/vnd.ms-powerpoint.addin.macroEnabled.12': 'ppam',
        'application/vnd.ms-powerpoint.presentation.macroEnabled.12': 'pptm',
        'application/vnd.ms-powerpoint.template.macroEnabled.12': 'potm',
        'application/vnd.ms-powerpoint.slideshow.macroEnabled.12': 'ppsm',
        "application/vnd.ms-excel": "xls",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template": "xltx",
        "application/vnd.ms-excel.sheet.macroEnabled.12": "xlsm",
        "application/vnd.ms-excel.template.macroEnabled.12": "xltm",
        "application/vnd.ms-excel.addin.macroEnabled.12": "xlam",
        "application/vnd.ms-excel.sheet.binary.macroEnabled.12": "xlsb",
    }

    def __init__(self, valid_extension='*', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_extensions = valid_extension

    def to_internal_value(self, data):
        if isinstance(data, str) and data.startswith('data'):
            # base64 encoded image - decode
            file_format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            file_format = file_format.replace("data:", "")
            ext = file_format.split('/')[-1]  # guess file extension
            if self.valid_extensions != '*':
                if file_format not in self.valid_extensions:
                    raise serializers.ValidationError('this file type is not supported. your file format {}'.format(file_format))
            try:
                decoded = base64.b64decode(imgstr)
            except TypeError:
                raise serializers.ValidationError('Not a valid file')

            if file_format in self.MIME_TYPE_MAP:
                ext = self.MIME_TYPE_MAP[file_format]
            data = ContentFile(decoded, name='temp.' + ext)

        return super().to_internal_value(data)
