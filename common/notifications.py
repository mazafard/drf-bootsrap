import datetime
import inspect

import pytz
from django.core.mail import EmailMultiAlternatives

from backend import settings

from common.api_connector import RestApiConnector


class SlackMessage(object):
    PRIORITY_SUCCESS = "success"
    PRIORITY_INFO = "info"
    PRIORITY_WARNING = "warning"
    PRIORITY_DANGER = "danger"

    @classmethod
    def send_to_slack(cls, message_body):
        url = settings.SLACK_REPORT_CHANNEL
        try:
            result = RestApiConnector.post(url=url, json=message_body, timeout=10)
        except Exception as e:
            print(e)
            return

        if result.status_code == 201:
            return True
        return False

    @classmethod
    def _send_message(cls, component, title, message_body, priority, trace=None):
        slack_message = cls.slack_message(component, title, message_body, priority,
                                          trace=trace)
        cls.send_to_slack(slack_message)

    @classmethod
    def slack_message(cls, component, title, message_body, priority, trace=None):
        tehran = pytz.timezone("Asia/Tehran")
        sa_time = datetime.datetime.now(tehran)
        today_name = sa_time.strftime("%A")
        now_date = sa_time.strftime("%Y-%m-%d")
        now_time = sa_time.strftime("%H:%M:%S")
        jalali_date = jdatetime.date.fromgregorian(date=sa_time).strftime("%Y-%m-%d")
        if not component:
            component_txt = ""
        else:
            component_txt = "ON : " + component
        if not title:
            title = priority

        # priority = Success! ,Info! ,Warning!  , Danger!

        fields = []
        if trace:
            message_body = "{} \n\n *Trace*\n```{}```".format(str(message_body), trace)
        data = {
            "username": "TSP Notification Center",
            "attachments": [
                {
                    "color": "#31708f",
                    "author_name": "Server Stage: {}".format(settings.SERVER_STAGE),
                    "text": str(
                        " *on* : `{}` | `{}` | `{}` | `{}` - _(Tehran timezone)_ ".format(
                            now_time,
                            today_name,
                            now_date,
                            jalali_date)),
                    "fallback": "Required plain-text summary of the attachment."
                },
                {
                    "fields": fields,
                    "author_name": component_txt,
                    "color": cls._get_alert_color(priority),
                    "text": str(message_body),
                    "fallback": "Required plain-text summary of the attachment.",
                    "title": title
                }
            ],
        }
        return data

    @classmethod
    def info(cls, message_body, component=None, title=None):
        return cls._send_message(component, title, message_body, cls.PRIORITY_INFO)

    @classmethod
    def success(cls, message_body, component=None, title=None):
        return cls._send_message(component, title, message_body, cls.PRIORITY_SUCCESS)

    @classmethod
    def warning(cls, message_body, component=None, title=None):
        return cls._send_message(component, title, message_body, cls.PRIORITY_WARNING)

    @classmethod
    def danger(cls, message_body, component=None, title=None, send_trace=True):
        trace = None
        if send_trace:
            cur_frame = inspect.currentframe()
            cal_frame = inspect.getouterframes(cur_frame, 2)
            cal_frame.pop(0)
            trace = "\n".join([i.strip() for i in cal_frame[0][4]])
            for frame in cal_frame:
                if frame[1].find("dist-packages") != -1:
                    break
                trace = "{trace}\n{address}:{line} {function}".format(trace=trace,
                                                                      address=frame[1],
                                                                      line=frame[2],
                                                                      function=frame[3])

        return cls._send_message(component, title, message_body, cls.PRIORITY_DANGER,
                                 trace)

    @staticmethod
    def _get_alert_color(alert):
        if alert == "success":
            return "#3c763d"
        elif alert == "info":
            return "#31708f"
        elif alert == "warning":
            return "#8a6d3b"
        elif alert == "danger":
            return "#a94442"


class SmsNotification(object):
    @staticmethod
    def send_token(country_code, cellphone, token):
        pass
            # api = @todo: api sms



class EmailNotification(object):
    @staticmethod
    def send_email(subject, to, message='', html_message=None, from_email=None, from_name=None,
                   attachments_address_list=None):

        if from_email and from_name:
            from_email = "{} <{}>".format(from_name, from_email)
        elif not from_email and from_name:
            from_email = "{} <{}>".format(from_name, settings.SYSTEM_MAIL_FROM)
        else:
            from_email = settings.SYSTEM_MAIL_FROM

        msg = EmailMultiAlternatives(
            subject,
            message,
            from_email,
            to if isinstance(to, (tuple, list)) else [to],
        )

        if html_message:
            msg.attach_alternative(html_message, "text/html")

        if attachments_address_list:
            for attachment in attachments_address_list:
                msg.attach_file(attachment)

        msg.send(fail_silently=True)
